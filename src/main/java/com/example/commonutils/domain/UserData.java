package com.example.commonutils.domain;

import com.example.commonutils.common.Excel;
import lombok.Data;

import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Table(name = "data")
public class UserData {
    @Id
    @Excel(name = "序号")
    Integer id;
    @Excel(name = "名字")
    String name;
    @Excel(name = "课程")
    String project;
    @Excel(name = "分数")
    Integer score;
}
