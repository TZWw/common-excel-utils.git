package com.example.commonutils.service.impl;

import com.example.commonutils.domain.UserData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.commonutils.mapper.*;
import java.util.List;

@Service
public class DataServiceImpl implements com.example.commonutils.service.DataService {
    @Autowired
    private DataMapper dataMapper;


    @Override
    public List<UserData> selectUserData() {
        return dataMapper.selectUserData();
    }

    @Override
    public List<UserData> userDataList() {
        return dataMapper.selectAll();
    }

    @Override
    public int importUserData(List<UserData> userDataList) {
        return dataMapper.insertList(userDataList);
    }


}
